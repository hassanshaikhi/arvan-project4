FROM nginx:latest
COPY ./nginx_conf/loadbalancer.conf /etc/nginx/nginx.conf
COPY .htpasswd /etc/nginx/.htpasswd
COPY ./certs /etc/nginx/certs 

