#!/bin/bash
#Get database container id
id=$(docker ps | grep web_db | awk {'print$1'})
#get mysql password
pass=$(docker exec $id env | grep MYSQL_ROOT_PASSWORD | cut -d= -f 2)
#get mysql database name
db=$(docker exec $id env | grep MYSQL_DATABASE | cut -d= -f 2)
docker exec $id /usr/bin/mysqldump -u root --password=$pass $db | gzip -9 > backup"`date +%Y-%m-%d`".sql.gz 